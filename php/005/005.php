<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

include '../Problem.php';
include '../Util.php';

$solution = function($bound){
	$solution = -1;
	if( !($bound > 0) ){
		return -1;
	}
	$range = range(2, $bound, 1);
	$primes = ProEuler\Util::findPrimeNumbersUpTo($bound);
	$factors = array_combine(array_values($primes), array_fill(0, count($primes), 0));
	foreach($range as $r){
		$tmpFactors = array_combine(array_values($primes), array_fill(0, count($primes), 0));
		$factorando = $r;
		foreach($tmpFactors as $prime => $countPrime){
			while( ($factorando % $prime) == 0){
				$tmpFactors[$prime]++;
				$factorando = $factorando/$prime;
			}
		}
		foreach ($factors as $prime => $primePower){
			$factors[$prime] = max([$primePower, $tmpFactors[$prime]]);
		}
	}
	$solution = 1;
	foreach($factors as $factor => $pow){
		$solution *= pow($factor, $pow);
	}
	return $solution;
};

$bound = 20;
$p = new ProEuler\Problem($solution, '#5');
$p->run($bound);
echo $p;
