<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

$primalityTest = function($pseudoPrime){
	$divisors = [];
	$maxTest = sqrt($pseudoPrime);
	$expectedDivisors = [1];
	for($i = 1; $i <= $maxTest; $i++){
		if( ($pseudoPrime % $i) == 0){
			$divisors[] = $i;
		}
	}
	return ($expectedDivisors === $divisors);
};

$naiveFindPrimeNumbersUntilUpperBound = function($number){
	$ret = [];
	$primeNumbers = array_fill(0, ($number+1), true);
	$primeNumbers[0] = $primeNumbers[1] = false;
	for($i = 2; $i <= $number; $i++){
		if($primeNumbers[$i]){
			for($j = $i * $i; $j <= $number; $j = $j+$i){
				$primeNumbers[$j] = false;
			}
		}
	}
	foreach($primeNumbers as $k=>$v){
		if ($v){
			$ret[] = $k;
		}
	}
	return $ret;
		
};



$solution = function($number) use($primalityTest, $naiveFindPrimeNumbersUntilUpperBound) {
	if( !($number > 0) ){
		return -1;
	}
	$maxSieve = ceil(bcsqrt($number));
	$primeNumbers = $naiveFindPrimeNumbersUntilUpperBound($maxSieve);
	$primeFactors = [];
	$maxPrime = -1;
	for($i = count($primeNumbers) - 1; $i >= 0; $i--){
		$p = $primeNumbers[$i];
		if( (bcmod($number, $p)) == 0){
			$primeFactors[] = $p;
			$maxPrime = $p;
			break;
		}
		else{
			
		}
	}
	if(empty($primeFactors)){
		$primTest = $primalityTest($number);
		if($primTest){
			$maxPrime = $number;
		}
	}
	return $maxPrime;
};

$target = '600851475143';
include '../Problem.php';
include '../Util.php';
$p = new ProEuler\Problem($solution, '#3');
$p->run($target);
echo $p;
