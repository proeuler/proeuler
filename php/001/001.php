<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include '../Problem.php';
include '../Util.php';

$solution = function($limit){
	$sum = 0;
	for($i=0; $i<$limit; $i++){
		if( (($i%3)==0) || (($i%5)==0)){
			$sum += $i;
		}
		
	}
	return $sum;
};
$limit = 1000;
$p = new ProEuler\Problem($solution, '#1');
$test = $p->testSolution(10, 23);
if($test){
	$p->run($limit);
}
echo $p;
