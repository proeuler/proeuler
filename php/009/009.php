<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

include '../Problem.php';
include '../Util.php';

$solution = function($bound){
	$solution = -1;
	$count = 0;
	for($a = 1 ; $a < $bound; $a++){
		for($b = ($a + 1); $b < ($bound - $a); $b++){
			$c = $bound - $a - $b;
			if( (pow($a, 2) + pow($b, 2)) == pow($c, 2) ){
				$solution = $a * $b * $c;
				return $solution;
			}
		}
	}
	return $solution;
};

$bound = 1000;
$p = new ProEuler\Problem($solution, '#');
$test = $p->testSolution((3+4+5), (3*4*5));
if($test){
	$p->run($bound);
}
echo $p;
