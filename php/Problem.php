<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

namespace ProEuler;

class Problem{
	private $_algorithm;
	private $_solution;
	private $_start;
	private $_end;
	private $_label;
	private $_test;
	private $_expected;
	
	public function __construct($closure, $label = '') {
		$this->_algorithm = $closure;
		$this->_label = $label;
		$this->_solution = null;
		$this->_start = 0;
		$this->_end = 0;
		$this->_test = false;
	}
	
	public function run($param = null){		
		$this->_test = false;
		$closure = $this->_algorithm;
		$this->_start = microtime(true);
		$this->_solution = $closure($param);
		$this->_end = microtime(true) - $this->_start;
		
	}
	
	public function testSolution($param, $expected){
		$this->run($param);
		$this->_test = true;
		$this->_expected = $expected;
		return ($this->_solution == $this->_expected);
	}
	
	
	
	public function __toString() {
		$label = $this->_label;
		if(!empty($this->_label)){
			$label = ' '.$label.' ';
		}
		$ret = "********** Solving Problem $label**********\n";
		if($this->_test){
			$ret .= "# SIMULATION  due to error \n";
		}
		$ret .= "# TIME  ".round($this->_end * 1000)."ms \n";
		$ret .= "# TIME  ".number_format(round(($this->_end),2), 2)."s \n";
		$ret .= "# SOL ".$this->_solution."\n";
		if($this->_test){
			$ret .= "# EXPECTED SOL ".$this->_expected."\n";
		}
		$ret .= $this->_solution."\n";
		return $ret;
	}
}