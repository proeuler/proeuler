<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

namespace ProEuler;

class Util{
	
	public static function findPrimeNumbersUpTo($number){
		$ret = [];
		$primeNumbers = array_fill(0, ($number+1), true);
		$primeNumbers[0] = $primeNumbers[1] = false;
		for($i = 2; $i <= $number; $i++){
			if($primeNumbers[$i]){
				for($j = $i * $i; $j <= $number; $j = $j+$i){
					$primeNumbers[$j] = false;
				}
			}
		}
		foreach($primeNumbers as $k=>$v){
			if ($v){
				$ret[] = $k;
			}
		}
		return $ret;
	}
	
	
	public static function sumFirstNNaturalNumbers($number){
		if(is_int($number) && $number > 0){
			return ($number * ($number + 1)) / 2;
		}
		return false;
	}
	
	public static function sumArray(array &$array){
		$sum = 0;
		foreach ($array as $v){
			$sum += $v;
		}
		return $sum;
	}
	
	
}