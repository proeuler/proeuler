<?php
/*
The MIT License (MIT)

Copyright (c) 2014 salvix

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 * 
 */

include '../Problem.php';
include '../Util.php';

$solution = function($bound){
	if( !($bound > 2) ){
		return -1;
	}
	$solution = -1;
	$primes = [3];
	$dividend = 5;
	$iteration = 0;
	while(count($primes) < $bound){
		$numberOfTests = count($primes);
		//echo "T: ".$dividend." ($numberOfTests tests reqd) : ";
		$maxPrime = sqrt($dividend);
		foreach($primes as $prime){
			if($prime > $maxPrime){
				$numberOfTests--;
			}
			else{
				if ( ($dividend % $prime != 0) ){
					$numberOfTests--;
				}
			}
		}
		//echo "FAILED: $numberOfTests tests\n";
		if($numberOfTests == 0){
			$primes[] = $dividend;
		}
		$dividend = $dividend + 2;
	}
	$solution = $primes[count($primes) - 2];
	return $solution;
	
};

$bound = 10001;
$p = new ProEuler\Problem($solution, '#7');
$p->run($bound);
echo $p;
